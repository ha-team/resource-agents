Source: resource-agents
Section: admin
Priority: optional
Maintainer: Debian HA Maintainers <debian-ha-maintainers@alioth-lists.debian.net>
Uploaders: Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
           Valentin Vidic <vvidic@debian.org>
Build-Depends: autoconf,
               automake,
               cluster-glue-dev,
               debhelper-compat (= 13),
               dh-exec,
               dh-python,
               docbook-xml,
               docbook-xsl,
               gawk,
               libglib2.0-dev,
               liblrm2-dev,
               libnet1-dev,
               libpils2-dev,
               libplumb2-dev,
               libplumbgpl2-dev,
               libqb-dev,
               libstonith1-dev,
               libtool,
               libxml2-utils,
               net-tools,
               pkgconf,
               python3,
               python3-googleapi,
               python3-pyroute2,
               python3-requests,
               python3-urllib3,
               shellcheck,
#              for systemd.pc:
               systemd-dev,
               xsltproc,
               xml-twig-tools,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Homepage: https://github.com/ClusterLabs/resource-agents
Vcs-Browser: https://salsa.debian.org/ha-team/resource-agents
Vcs-Git: https://salsa.debian.org/ha-team/resource-agents.git

Package: resource-agents
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${python3:Depends},
 bc, cluster-glue, gawk, psmisc
Recommends: libxml2-utils, net-tools, python3-googleapi
Provides: resource-agents-dev
Description: Cluster Resource Agents
 This package contains cluster resource agents (RAs) compliant with the Open
 Cluster Framework (OCF) specification, used to interface with various services
 in a High Availability environment managed by the Pacemaker resource manager.
 .
 Agents included:
 ${agents}

Package: ldirectord
Architecture: all
Depends: ipvsadm,
         libauthen-radius-perl,
         libcrypt-ssleay-perl,
         libdbi-perl,
         libdigest-hmac-perl,
         libdigest-md5-perl,
         libio-socket-inet6-perl,
         libmail-pop3client-perl,
         libmailtools-perl,
         libnet-dns-perl,
         libnet-imap-simple-perl,
         libnet-imap-simple-ssl-perl,
         libnet-ldap-perl,
         libnet-perl,
         libsocket6-perl,
         libwww-perl,
         ${misc:Depends},
         ${perl:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Monitors virtual services provided by LVS
 ldirectord is a stand-alone daemon to monitor real servers behind
 virtual services provided by The Linux Virtual Server (LVS).
